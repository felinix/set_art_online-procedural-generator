// *******************************************************
// Procedural Generation - RPG Maker MV
// By Kyle Thomson-Diks
// Updated Mar 25, 2016
// Version 1.0
// *******************************************************

/*:
 * @plugindesc Provides the logic for randomly generating dungeon layouts.
 * @author Kyle Thomson-Diks
 *
 * @help This plugin provides plugin commands.
 *  
 * @param weatherKeyword
 * @type integer
 * @desc refers to the weather keyword, used in generating a random number of rooms.
 * 
 * @param timeKeyword
 * @type integer
 * @desc refers to the time keyword, used in determining the tiles to use for the dungeon.
 * 
 * @param fieldKeyword
 * @type integer
 * @desc refers to the field keyword, used in determining the size of the map.
 *
 * @param dungeonFloor
 * @type integer
 * @desc contains the 4 digit number referring to the tile ID used for the dungeon floor.
 *
 * @param dungeonWall
 * @type integer
 * @desc contains the 4 digit number referring to the tile ID used for the dungeon walls.
 *
 * @param dungeonCeiling
 * @type integer
 * @desc contains the 4 digit number referring to the tile ID used for the dungeon ceiling.
 *
 * @param dungeonWidth
 * @type integer
 * @desc the width of the dungeon map.
 *
 * @param dungeonHeight
 * @type integer
 * @desc the height of the dungeon map.
 *
 * @param Rooms
 * @type array of Room
 * @desc used to hold all room arrays generated by this plugin.
 *
 *
 * HOW TO USE:
 *	Install the plugin and use the plugin command: "Generate"  
 *
 *	After any event that changes the scene away from the map, call the plugin command: "DrawMap".
 *	This includes, but is not limited to returning from a random encounter, transfer events, and 
 *	scene changes including closing a menu (debug window included).
 *
 * 	Used in conjunction with the mv_battle_result_switches.js plugin by Shaz in order to redraw 
 *	the map after returning from a random encounter. Set up a common event such as the example 
 *	below for each of the battle end switches and make sure they're set to Autorun.
 *
 *	If : Battle Won Switch is ON 
 *		Plugin Command DrawMap
 *		Control Switches #000X Battle Won Switch = OFF
 *	End
 *
 *
 * @DESCRIPTION:
 *	This version of procedural generation uses a similar concept as the map generator from the 
 *	video game series: .hack, where the combination of 3 keywords determines map properties.
 *	
 *	As such, this plugin assumes the same about your RPG. Without modification, this generator
 * 	assumes the storage of 3 keywords associated with Weather, Time-of-day and Field type. 
 *
 *	Weather is used in determining the number of rooms in the dungeon and room size.
 *	Time is used in determining the type of tiles to use.
 *	Field is used in determining the number of rooms and the size of the map.
 *	
 *	Map objects are NOT generated through this script. Empty maps are created in the RPG Maker MV 
 *	client and the map data is modified in the script below by navigating the mapData array and 
 *	assigning tile values for floor, wall and ceiling tiles. 
 *
 *
 * @ KNOWN ISSUES:
 *	1:
 *	The redraw problem mentioned above. This plugin does not account for the rendering functions 
 *	the RPG Maker engine. As such, any event that would change the context away from the map will
 *	cause the map to be erased from the screen. The map must be redrawn after any event that causes
 *	this change in context. 
 *
 *	2:
 *	Wall and ceiling tiles are not supported properly by the procedural generator because of the 
 *	nature of RPG Maker's engine and layering issues. Adding code to place wall and ceiling tiles
 *	creates impassable maps because hallway (floor) tiles do not overwrite wall or ceiling tiles
 *	when they're laid. Adding functionality to the functions below to support wall and ceiling 
 *	tiles is currently beyond the scope of this plugin. 
 *
 *
 * @TERMS OF USE
 *	This plugin is free to use in your own project for commercial or non-commercial purposes. 
 *	If you are going to use this plugin I'd ask that you mention it in the credits of your game
 *	and give credit where credit is due. Let me know. I'd love to know that you found it helpful.
 *
 *
 * @SUPPORT
 *	I will not be providing support for this plugin for modifications or changes to the code. If
 *	you are going to change the code in any way I would ask that you mention so in the credits of 
 * 	your game as per the terms of use. 
 *
 *	If you have any comments or suggestions for improvement, however, I would love to hear them. 
 *	This is the first plugin I've ever written for the RPG Maker engine so it is a labour of 
 *	experimentation.
 *
 *
 *	Thank you for using the ProceduralGeneration plugin developed by Kyle Thomson-Diks, 2016.
 *
 */

/*
 *********************************
 * Plugin Command Override:
 *	Uncomment this code to provide the plugin commands to your game. 
 *	Call Generate in order to start the generation process.
 *	Call DrawMap in order to redraw the map after a battle or scene change.
 *	Call ZeroMap in order to clear the dataMap.data array and prevent memory leaks.
 *
 *	var interpreterCommand = Game_Interpreter.prototype.pluginCommand;
 *	Game_Interpreter.prototype.pluginCommand = function(command, args) {
 *		interpreterCommand.apply(this);
 *		if (command === 'Generate') 
 *			GenerateMap();
 *		else if (command === 'DrawMap') 
 *			DrawMapData();
 *		else if (command === 'ZeroMap') 
 *			ZeroMapData();		
 *		return;
 *	};
 **********************************
 */

/*
 * Global Parameter list:
 *	Numeric values are the variable ID the variable resided in while in
 *	use in SET Art Online. Values will change for any other game using 
 *	this script. Change the values to represent the variable IDs unique 
 * 	to you.
 *
 *	Many of these values are global in order to support redrawing maps 
 *	after a context switching event.
 */
var parameters = PluginManager.parameters('ProceduralGenerationVariables');
var weatherKeyword = Number(parameters['Weather Keyword'] || 1);		// integer referring to the weather keyword
var timeKeyword = Number(parameters['Time Keyword'] || 2);				// integer referring to the time keyword
var fieldKeyword = Number(parameters['Field Keyword'] || 3);			// integer referring to the field keyword
var dungeonMapID = Number(parameters['Map ID'] || 6);					// the map ID of the dungeon
var dungeonWidth = Number(parameters['Dungeon Width'] || 7);			// map width
var dungeonHeight = Number(parameters['Dungeon Height'] || 8);			// map height
var dungeonFloor = Number(parameters['Dungeon Floor Tile'] || 9); 		// holds the tile ID of the dungeon floor
var dungeonWall = Number(parameters['Dungeon Wall Tile'] || 10); 		// holds the tile ID of the dungeon wall
var dungeonCeiling = Number(parameters['Dungeon Ceiling Tile'] || 11);	// holds the tile ID of the dungeon cailing
var bossEventX = Number(parameters['Boss Event X'] || 14);				// holds the X coordinate of the boss battle
var bossEventY = Number(parameters['Boss Event Y'] || 15);				// holds the Y coordinate of the boss battle
var Rooms = [];															// an array to hold all generated rooms.

// Enum type array of tile IDs
TileTypes = {
	GrassFloor 		: 2892,  
	GrassWall 		: 6474,
	GrassCeiling 	: 6108,
	StoneFloor		: 3245,
	StoneWall		: 6331,
	StoneCeiling	: 5982,
	DesertFloor		: 2861,
	DesertWall		: 6283,
	DesertCeiling	: 5934,
	CrystalFloor	: 3293,
	CrystalWall		: 6523,
	CrystalCeiling	: 6174,
	IceFloor		: 4013,
	IceWall			: 6431,
	IceCeiling		: 6074,
	BlackEmpty		: 1536,
	BossFloor		: 1654
};

// Admin function. Call when generating a new dungeon from scratch. 
function GenerateMap() {		
	try {		
		// Get the time, weather and field variables from the system.
		var time = $gameVariables.value(timeKeyword)
		var weather = $gameVariables.value(weatherKeyword);
		var field = $gameVariables.value(fieldKeyword);
		// Get the map information from the system.
		var mapWidth = $gameVariables.value(dungeonWidth);
		var mapHeight = $gameVariables.value(dungeonHeight);
		var mapId = $gameVariables.value(dungeonMapID);
		
		// Determine the tiles to use and the number of rooms to generate. 
		this.DetermineTiles(time);
		var numOfRooms = this.DetermineNumOfRooms(weather, field);
		
		// Clear any left over map data from previous runs, if they exist. 
		ZeroMapData();
		if (Rooms.length > 0) { Rooms = []; }
		
		// Generate a number of rooms and add them to the Rooms array.
		// Once a valid room is generated, break from the Generation loop
		// and start the next room. Only try to generate a failed room 5 
		// times to prevent near infinite generation times. 
		for (i = 0; i < numOfRooms; i++) {
			for (j = 0; j < 5; j++) {
				var _Room = this.GenerateNewRoom(mapWidth, mapHeight, weather);					
				if (this.ValidateRoom(_Room, Rooms, mapWidth, mapHeight)) { 
					Rooms.push(_Room);
					break;
				}
			}				
		}
		
		// Make a 10x10 room at the map origin as first room in Rooms array.
		var _defaultRoom = [0, 0, 10, 10]; 							
		Rooms.unshift(_defaultRoom); 								
		
		// Make a 5x5 room at the bottom left corner as last room in Rooms array. 
		var _bossRoom = [(mapWidth - 6), (mapHeight - 6), 5, 5];
		Rooms.push(_bossRoom);	
		
		// Debug code. Uncomment to see the contents of the Rooms array.
		// PrintRoomData(Rooms);
		
		// Draw the map data to the $dataMap.data array.
		this.DrawMapData();
	} catch (e) {
		console.log("GenerateMap " + e.Message);
	}
};

// Admin function. Call to draw the map using the generated map data.
function DrawMapData() {
	try {
		// Get these two values again to facilitate the Redraw method.
		var mapWidth = $gameVariables.value(dungeonWidth);
		var mapHeight = $gameVariables.value(dungeonHeight);
		
		FillInMapFloor(Rooms, mapWidth, mapHeight);	
		FillInMapHorizontalHallways(Rooms, mapWidth, mapHeight);
		FillInMapVerticalHallways(Rooms, mapWidth, mapHeight);
	} catch (e) {
		console.log("DrawMapData " + e.Message);
	}
};

// Zeroes the map data from the $dataMap.data array.
function ZeroMapData () {
	for (i = 0; i < dungeonWidth * dungeonHeight; i++) { $dataMap.data[i] = 0; }
}

// Use the time keyword to select which of the tile sets to use. 
function DetermineTiles(time) {
	switch (time) {
		case 1:
			$gameVariables.setValue(dungeonFloor, TileTypes.GrassFloor);
			$gameVariables.setValue(dungeonWall, TileTypes.GrassWall);
			$gameVariables.setValue(dungeonCeiling, TileTypes.GrassCeiling);
			break;
			
		case 2:
			$gameVariables.setValue(dungeonFloor, TileTypes.StoneFloor);
			$gameVariables.setValue(dungeonWall, TileTypes.StoneWall);
			$gameVariables.setValue(dungeonCeiling, TileTypes.StoneCeiling);
			break;
		
		case 3:
			$gameVariables.setValue(dungeonFloor, TileTypes.DesertFloor);
			$gameVariables.setValue(dungeonWall, TileTypes.DesertWall);
			$gameVariables.setValue(dungeonCeiling, TileTypes.DesertCeiling);
			break;
			
		case 4:
			$gameVariables.setValue(dungeonFloor, TileTypes.CrystalFloor);
			$gameVariables.setValue(dungeonWall, TileTypes.CrystalWall);
			$gameVariables.setValue(dungeonCeiling, TileTypes.CrystalCeiling);
			break;
		
		case 5:
			$gameVariables.setValue(dungeonFloor, TileTypes.IceFloor);
			$gameVariables.setValue(dungeonWall, TileTypes.IceWall);
			$gameVariables.setValue(dungeonCeiling, TileTypes.IceCeiling);
			break;
	}	
};

// return a number relative to weather and field values.
function DetermineNumOfRooms(weather, field) {
	return (weather + field) * 2;
}

// Return a random number between the Bottom and Top values inclusive. 
function GetRandomNumber(_bottom, _top) {
	return Math.floor((Math.random() * (_top - _bottom + 1)) + _bottom);
}

// Create a set of room coordinates that do not collide with the landing zone room.
function GenerateNewRoom(dungeonWidth, dungeonHeight, weather) {
	var _bottom = weather + 8;
	var _top = weather + 15;
	var roomWidth = this.GetRandomNumber(_bottom, _top); 	// Get random width
	var roomHeight = this.GetRandomNumber(_bottom, _top); 	// Get random height
	
	do {
		// Get x coordinate offset by 1 from left, offset by 1 + width from right
		var roomX = this.GetRandomNumber(1, dungeonWidth - (1 + roomWidth)); 
		// Get y coordinate offset by 3 from top, 1 + height from bottom
		var roomY = this.GetRandomNumber(3, dungeonHeight - (1 + roomHeight)); 
	} while (roomX <= 10 && roomY <= 10) // generate a new origin if the origin conflicts with the default 10x10 room.
	
	// new room array contains X-Origin, Y-Origin, Width, Height
	var room = [roomX, roomY, roomWidth, roomHeight];
	return room;
}

// validate that a generated room is allowed against the already created rooms. 
function ValidateRoom(newRoom, RoomList, dungeonWidth, dungeonHeight) {	
	// Room array = [0]: x, [1]: y, [2]: width, [3]: height
	var valid = true;
	
	try {
		var newRoomTopLeftX = newRoom[0]; 						// x
		var newRoomTopLeftY = newRoom[1]; 						// y
		var newRoomTopRightX = newRoom[0] + newRoom[2];			// x + width
		var newRoomTopRightY = newRoom[1];						// y
		var newRoomBotLeftX = newRoom[0];						// x
		var newRoomBotLeftY = newRoom[1] + newRoom[3];			// y + height		
	
		for (i = 0; i < RoomList.length; i++) {
		
			var oldRoomTopLeftX = RoomList[i][0]; 					// x
			var oldRoomTopLeftY = RoomList[i][1]; 					// y
			var oldRoomTopRightX = RoomList[i][0] + RoomList[i][2];	// x + width
			var oldRoomTopRightY = RoomList[i][1];					// y
			var oldRoomBotLeftX = RoomList[i][0];					// x
			var oldRoomBotLeftY = RoomList[i][1] + RoomList[i][3];	// y + height	
			
			// The following diagrams are tested for both the new room and old room configurations
			
			// (0,0)----------------------------> x
			//	|	o-------------------o
			//	|	|					|
			//	|	|					|
			//	|	|		b 1			|
			//	|	|					|
			//	|	|  			x-----------o
			//	|	|			|			|
			//	|	|			|	 b 2	|
			//	|	o-----------|			|
			//	|				|			|
			//	|				o-----------o
			//	|
			// 	V
			//	y
			//
			// check to see that a box's origin is not inside another box's space.
			//
			if (newRoomTopLeftX <= oldRoomTopRightX && newRoomTopLeftX >= oldRoomTopLeftX &&
				newRoomTopLeftY >= oldRoomTopLeftY && newRoomTopLeftY <= oldRoomBotLeftY) {
					valid = false;
			}
			else if (oldRoomTopLeftX <= newRoomTopRightX && oldRoomTopLeftX >= newRoomTopLeftX &&
				oldRoomTopLeftY <= newRoomBotLeftY && oldRoomTopLeftY >= newRoomBotLeftY) {
					valid = false;
			}
			
			// (0,0)----------------------------> x
			//	|				o-----------o
			//	|				|			|
			//	|				|	b 1		|
			//	|	o-------------------x	|
			//	|	|					|	|
			//	|	|					|---o
			//	|	|		b 2			|
			//	|	|					|
			//	|	|  					|
			//	|	|					|
			//	|	|					|
			//	|	o-------------------o
			//	|
			// 	V
			//	y
			//
			// check to see if a room's top right corner is inside another room's space
			//
			else if (newRoomTopRightX >= oldRoomTopLeftX && newRoomTopRightX <= oldRoomTopRightX &&
				newRoomTopRightY >= oldRoomTopLeftY && newRoomTopRightY <= oldRoomBotLeftY) {
					valid = false;
			}
			else if (oldRoomTopRightX >= newRoomTopLeftX && oldRoomTopRightX <= newRoomTopRightX &&
				oldRoomTopRightY >= newRoomTopLeftY && oldRoomTopRightY <= newRoomBotLeftY) {
					valid = false;
			}
			
			// (0,0)----------------------------> x
			//	|				o-----------o
			//	|				|			|
			//	|				|	b 2		|
			//	|	o-----------|			|
			//	|	|			|			|
			//	|	|			x-------|---o
			//	|	|		b 1			|
			//	|	|					|
			//	|	|  					|
			//	|	|					|
			//	|	|					|
			//	|	o-------------------o
			//	|
			// 	V
			//	y
			//
			// see if a room's bottom left corner is inside another room's space
			//
			else if (oldRoomBotLeftX >= newRoomTopLeftX && oldRoomBotLeftX <= newRoomBotLeftX &&
				oldRoomBotLeftY >= newRoomTopLeftY && oldRoomBotLeftY <= newRoomBotLeftY) {
					valid = false;
			}
			else if (newRoomBotLeftX >= oldRoomTopLeftX && newRoomBotLeftX <= oldRoomBotLeftX &&
				newRoomBotLeftY >= oldRoomTopLeftY && newRoomBotLeftY <= oldRoomBotLeftY) {
					valid = false;
			}
			
			// (0,0)----------------------------> x
			//	|		o-----------o
			//	|		|			|
			//	|		|	b 2		|
			//	|	o---|-----------|---o
			//	|	|	|	 		|   |		
			//	|	|	|			|	|		
			//	|	|	|	b 1		|	|
			//	|	|	|			|	|
			//	|	|  	|			|	|
			//	|	|	|			|	|
			//	|	|	|			|	|
			//	|	o---|-----------|---o
			//	|		|			|
			//	|		o-----------o
			// 	V
			//	y
			//
			// see if either room overlaps without corners in each other's space.
			//
			else if (newRoomTopLeftX <= oldRoomTopLeftX && newRoomTopRightX >= oldRoomTopRightX &&
				newRoomTopLeftY >= oldRoomTopLeftY && newRoomBotLeftY <= oldRoomBotLeftY) {
					valid = false;
			}
			else if (oldRoomTopLeftX <= newRoomTopLeftX && oldRoomTopRightX >= newRoomTopRightX &&
				oldRoomTopLeftY >= newRoomTopLeftY && oldRoomBotLeftY <= newRoomBotLeftY) {
					valid = false;
			}
			else if (newRoomTopLeftX >= oldRoomTopLeftX && newRoomTopRightX <= oldRoomTopRightX &&
				newRoomTopLeftY <= oldRoomTopLeftY && newRoomBotLeftY >= oldRoomBotLeftY) {
					valid = false;
			}
			else if (oldRoomTopLeftX >= newRoomTopLeftX && oldRoomTopRightX <= newRoomTopRightX &&
				oldRoomTopLeftY <= newRoomTopLeftY && oldRoomBotLeftY >= newRoomBotLeftY) {
					valid = false;
			} // end if 
		} // end for		
	} catch (e) {
		console.log("ValidateRoom: " + e.Message);
		return false;
	} // end try		
	
	return valid;
};

// Draw the floor tiles of all rooms.
function FillInMapFloor(Rooms, dungeonWidth, dungeonHeight) {	
	try {
		var floor = $gameVariables.value(dungeonFloor);
		var ceiling = $gameVariables.value(dungeonCeiling);
		var wall = $gameVariables.value(dungeonWall);
	
		// Room array = [0]: x, [1]: y, [2]: width, [3]: height
		// for each room
		for (i = 0; i < Rooms.length; i++) {
			// for each row in the dungeon			
			for (j = 0; j < dungeonHeight; j++) { 
				// if the row is between the top and bottom X value of the room.			
				if (j >= Rooms[i][1] && j < Rooms[i][1] + Rooms[i][3]) { 	
					// change the indices between the left and right corners to floor tiles.
					for (k = (j * dungeonWidth) + Rooms[i][0]; k < (j * dungeonWidth) + Rooms[i][0] + Rooms[i][2]; k++) { 
						$dataMap.data[k] = floor;
					}
				}		
			}
		}	
	} catch (e) {
		console.log("FillInMapFloor: " + e.Message);
	}
}

// Draw the floor tiles for all horizontal hallways.
function FillInMapHorizontalHallways(Rooms, dungeonWidth, dungeonHeight) {	
	var floor = $gameVariables.value(dungeonFloor);
	var ceiling = $gameVariables.value(dungeonCeiling);
	var wall = $gameVariables.value(dungeonWall);
		
	try {
		// for each room
		for (i = 1; i < Rooms.length; i++) { 									
		
			// calculate the midpoints for 
			var firstRoomMidpointX = Rooms[i - 1][0] + Math.floor(Rooms[i - 1][2] / 2);
			var firstRoomMidpointY = Rooms[i - 1][1] + Math.floor(Rooms[i - 1][3] / 2);
			
			var secondRoomMidpointX = Rooms[i][0] + Math.floor(Rooms[i][2] / 2);
			var secondRoomMidpointY = Rooms[i][1] + Math.floor(Rooms[i][3] / 2);
		
			// find the left most midpoint
			var leftIndex;
			var rightIndex;
			if (firstRoomMidpointX < secondRoomMidpointX) {
				leftIndex = firstRoomMidpointX;
				rightIndex = secondRoomMidpointX;
			}
			else {
				leftIndex = secondRoomMidpointX;
				rightIndex = firstRoomMidpointX;
			}
			
			// find the top most midpoint
			var topIndex;
			var bottomIndex;
			if (firstRoomMidpointY < secondRoomMidpointY) {
				topIndex = firstRoomMidpointY;
				bottomIndex = secondRoomMidpointY;
			}
			else {
				topIndex = secondRoomMidpointY;
				bottomIndex = firstRoomMidpointY;
			}
				
			// draw the hallway from left to right all the time.
			// j is the dungeon row
			var index = 0;
			for (j = 0; j <= dungeonHeight; j++) {
				// for each tile from the x coordinate to the y coordinate at the end of the room.
				if (j === topIndex) { 						
					for (k = index + leftIndex; k <= index + rightIndex; k++) { 
						$dataMap.data[k] = floor; 						
					}
					break;
				}
				index += dungeonWidth; // increase the index of the calculated spot by the width of the dungeon map.		
			}
		}	
	} catch (e) {
		console.log("FillInMapHorizontalHalways: " + e.Message);
	}
}

// Draw the floor tiles for all vertical hallways. 
function FillInMapVerticalHallways(Rooms, dungeonWidth, dungeonHeight) {
	var floor = $gameVariables.value(dungeonFloor);
	var ceiling = $gameVariables.value(dungeonCeiling);
	var wall = $gameVariables.value(dungeonWall);
		
	try {
		// for each room
		for (i = 1; i < Rooms.length; i++) { 									
		
			// calculate the midpoints for 
			var firstRoomMidpointX = Rooms[i - 1][0] + Math.floor(Rooms[i - 1][2] / 2);
			var firstRoomMidpointY = Rooms[i - 1][1] + Math.floor(Rooms[i - 1][3] / 2);
			
			var secondRoomMidpointX = Rooms[i][0] + Math.floor(Rooms[i][2] / 2);
			var secondRoomMidpointY = Rooms[i][1] + Math.floor(Rooms[i][3] / 2);
		
			// find the left most midpoint
			var leftIndex;
			var rightIndex;
						
			// find the top most midpoint
			var topIndex;
			var bottomIndex;
			if (firstRoomMidpointY < secondRoomMidpointY) {
				topIndex = firstRoomMidpointY;
				bottomIndex = secondRoomMidpointY;
				leftIndex = secondRoomMidpointX;
			}
			else {
				topIndex = secondRoomMidpointY;
				bottomIndex = firstRoomMidpointY;
				leftIndex = firstRoomMidpointX;
			}
				
			// draw the hallway from top to bottom.
			// j is the dungeon row
			var index = 0;
			for (j = 0; j <= dungeonHeight; j++) {
				if (j >= topIndex && j <= bottomIndex) { 
					$dataMap.data[(j * dungeonWidth) + leftIndex] = floor; 
				}		
			}
		}	
	} catch (e) {
		console.log("FillInMapVerticalHalways: " + e.Message);
	}
}

// Debug function. Prints every index of the current room to show tile IDs.
function PrintMapData(dungeonHeight, dungeonWidth){
	for (i = 0; i < dungeonHeight * dungeonWidth; i += dungeonWidth) {
		var row = $dataMap.data.slice(i, i + dungeonWidth);
		row.forEach(function(unit) { unit = unit.padZero(4); });
		console.log(i / dungeonWidth + ': ' + row.join('|'));
	}
}

// Debug function. Prints the Rooms array
function PrintRoomData(Rooms) {
	for (i = 0; i < Rooms.length; i++) { console.log(Rooms[i].join('|')); }
}